#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct ptr
        {
        int  rec;
        struct  ptr *lp;
        struct  ptr *rp;
        } TREE;

TREE *treeLeaf (int);
TREE *newnode (int, TREE *, TREE *);

void inOrder (TREE *);
void preOrder (TREE *);
void postOrder (TREE *);

int height (TREE *);
void printLevelOrder (TREE *);
void printGivenLevel (TREE *, int);


void deleteTree(TREE* root);

int main (void)
{
    TREE *root=(TREE *) NULL;
    char akin[5];
    int  ak, sum=0, hgt;

    gets (akin);
    ak = atoi (akin);
    root = newnode (ak, root, root);
    gets (akin);
    ak=atoi (akin);
    while (ak > 0) 
    {
          newnode (ak, root, root);
          gets (akin);
          ak=atoi (akin); 
    }
    puts ("Inorder");
    inOrder (root);
    printf ("\n");
    puts ("Preorder");   
    preOrder (root);
    printf ("\n");
    puts ("Postorder");   
    postOrder (root );
    printf ("\n");
    hgt = height (root);
    printf ("\nYPSOS TOY DENTROY = %5d\n", hgt);
    puts ("DIASXISH ANA EPIPEDO");   
    printLevelOrder (root);
    printf ("\n");
    deleteTree(root);
    system ("pause");
    return 1;
}



void inOrder (TREE *q) 
{
     if (q!=NULL) 
     {
        inOrder (q->lp);
        printf ("%d  ",q->rec);
        inOrder (q->rp); 
     }
}


void preOrder (TREE *q)
{
     if (q!=NULL) 
     {
        printf ("%d  ", q->rec);
        preOrder (q->lp);
        preOrder (q->rp); 
     }
}


void postOrder (TREE *q) 
{
     if (q!=NULL) 
     {
        postOrder (q->lp);
        postOrder (q->rp);
        printf ("%d  ", q->rec); 
     }
}



TREE *treeLeaf (int num) 
{
     TREE *p;
     p = (TREE *) malloc (sizeof (TREE));
     p->rec = num;
     p->lp = NULL;
     p->rp = NULL;
     return (p);
}



TREE *newnode (int num, TREE *pq, TREE *q) 
{
     if (q!=NULL) 
     {
        if (num < q->rec)
	        q = newnode (num, q, q->lp);
        else
            q = newnode (num, q, q->rp); 
     }
     else 
     {
          q = treeLeaf (num);
          if (pq!=NULL) 
          {
			if (num < pq->rec)
               pq->lp = q;
			else
               pq->rp = q; 
           }
      }
return (q);
}




int height(TREE* node)
{
    int lheight, rheight;

    if (node==NULL)
    return 0;
    else
    {

    lheight = height (node->lp);
    rheight = height (node->rp);


    if (lheight > rheight)
    return (lheight+1);
    else
    return (rheight+1);
    }
} 






void printLevelOrder (TREE* root)
{
     int h = height (root);
     int i;
     for (i=1; i<=h; i++)
     {
     printGivenLevel (root, i);
     printf("\n");
     }
}     


void printGivenLevel (TREE* root, int level)
{
     if (root == NULL)
     return;
     if (level == 1)
        printf ("%5d", root->rec);
     else
     if (level > 1) 
     {
     printGivenLevel (root->lp, level-1);
     printGivenLevel (root->rp, level-1);
     }
}


void deleteTree(TREE* root)
{
    if (root == NULL) return;   
    if(root->lp)
    {
           free(root);       
                  }
    root=root->lp;
}